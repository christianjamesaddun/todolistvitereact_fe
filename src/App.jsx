import { useState, useEffect } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import axios from "axios";
// import "./App.css";

const backend_url = "http://localhost:3000/todo_list";

function App() {
  const [todo_data, setTodoData] = useState([]);
  const [todolist_title, setTodolistTitle] = useState("");

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios
      .get(`${backend_url}/get_data`)
      .then((item) => {
        setTodoData(item.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const addTodoData = () => {
    axios
      .post(`${backend_url}/add_data`, { todolist_title })
      .then((item) => {
        setTodoData(item.data);
        getData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const editData = (id) => {
    axios
      .put(`${backend_url}/update_data/${id}`)
      .then((item) => {
        getData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  return (
    <div>
      <h1>To do list app</h1>
      {todo_data.map((item) => {
        if (!item.isCompleted) {
          return (
            <div>
              <input
                type="checkbox"
                onChange={(e) => {
                  editData(item.todo_id);
                }}
              />{" "}
              {item.todolist_title}
            </div>
          );
        }
      })}

      <input
        type="button"
        value="Add"
        onClick={() => {
          addTodoData();
        }}
      />
      <input
        type="text"
        onChange={(e) => {
          setTodolistTitle(e.target.value);
        }}
      />
    </div>
  );
}

export default App;
